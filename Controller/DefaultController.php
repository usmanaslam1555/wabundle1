<?php

//plugins/WABundle/Controller/DefaultController.php

namespace MauticPlugin\WABundle\Controller;

use Mautic\ApiBundle\Controller\CommonApiController;
use Mautic\CoreBundle\Controller\FormController;
use MauticPlugin\WABundle\EventListener\LeadSubscriber;

class DefaultController extends FormController
{
    public function defaultAction()
    {
    }

    public function adminAction()
    {

        // init event dispatcher and register subscriber
        $dispatcher = new EventDispatcher();
        $subscriber = new LeadSubscriber();
        $dispatcher->addSubscriber($subscriber);
 
        // dispatch
        $dispatcher->dispatch(LeadEvents::TIMELINE_ON_GENERATE, new LeadTimelineEvent());

        
        return $this->delegateView(
            array(
                'viewParameters'  => array("name"=>"Deepak", "age"=>21, "marks"=>75),
                'contentTemplate' => 'WABundle:Response:index.html.php'
            )
        );
    }
}

class ApiController extends CommonApiController
{
    public function adminAction()
    {
        $cars = array("name"=>"Deepak", "age"=>21, "marks"=>75);
        return $cars;
    }
}
