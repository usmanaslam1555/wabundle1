<?php
// plugins/HelloWorldBundle/EventListener/LeadSubscriber.php

namespace MauticPlugin\WABundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Mautic\LeadBundle\Event\LeadTimelineEvent;
use Mautic\LeadBundle\LeadEvents;

/**
 * Class LeadSubscriber
 */
class LeadSubscriber implements EventSubscriberInterface
{
    public function helloWorld(): string {
        $messages = [
            'You did it! You updated the system! Amazing!',
            'That was one of the coolest updates I\'ve seen all day!',
            'Great work! Keep going!',
        ];

        $index = array_rand($messages);

        return $messages[$index];
    }
    
    public static function getSubscribedEvents()
    {
        return [
            LeadEvents::TIMELINE_ON_GENERATE => 'onTimelineGenerate'
        ];
    }

    public function onTimelineGenerate(LeadTimelineEvent $event)
    {
        $leadId = $event->getLead()->getId();
        $url = 'https://envh0ojql3zwp7.m.pipedream.net';
        $data = array('leadId' => $leadId);
        $options = array(
            'http' => array(
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'method'  => 'POST',
            'content' => http_build_query($data)
            )
        );
        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        $event->addEvent(
            [
                // Event key type
                'event'           => 'message.receive',
                // Event name/label - can be a string or an array as below to convert to a link
                'eventLabel'      => "contact send message",
                // Translated string displayed in the Event Type column
                'eventType'       => 'Type: Message Receipt from contact',
                // \DateTime object for the timestamp column
                'timestamp'       => '2021-06-24T11:27:08+0000',
                // Font Awesome class to display as the icon
                'icon'            => 'fa-envelope'
            ]
        );
    }
}