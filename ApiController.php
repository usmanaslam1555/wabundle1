<?php

// plugins/HelloWorldBundle/Controller/ApiController.php

namespace MauticPlugin\WABundle\Controller\ApiController;

use FOS\RestBundle\Util\Codes;
use Mautic\ApiBundle\Controller\CommonApiController;

class ApiController extends CommonApiController
{
    public function adminAction()
    {
        $cars = array("Volvo", "BMW", "Toyota");
    
        return $cars;
    }
}