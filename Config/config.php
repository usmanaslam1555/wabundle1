<?php

// plugins/WABundle/Config/config.php

return array(
    'name'        => 'WABundle',
    'description' => 'Add here description of plugin',
    'author'      => 'Avinash Dalvi', // Change this to  plugin author
    'version'     => '1.0.0', // Change this version to your appropriate version
    'services'    => array(
        'events' => array(
            'plugin.wa.leadbundle.subscriber' => array(
                'class' => 'MauticPlugin\WABundle\EventListener\LeadSubscriber'
            )
        )
        // 'other' => [
        //         'mautic.sms.transport.smsapi' => [
        //         'class' => SmsapiApi::class,
        //         'arguments' => [
        //             'mautic.sms.smsapi.plugin'
        //             // 'mautic.sms.smsapi.gateway',
        //             // 'monolog.logger.mautic',
        //         ],
        //         'tag' => 'mautic.sms_transport',
        //         'tagArguments' => [
        //             'integrationAlias' => 'MauticSmsapiConst',
        //         ],
        //     ]
        // ]
    ),
    'routes'   => array(
      'api' => array(
            'plugin_helloworld_api' => array(
                'path'       => '/get',
                'controller' => 'WABundle:Default:admin',
                'method'     => 'GET'
            )
        )
    )
);